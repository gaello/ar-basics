# AR Basics with ARFoundation in Unity

So you are looking for information how to use ARFoundation in Unity? Great! This repository is exactly what you need! This is basic setup of scene and project so you can adjust it to your needs.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/15/building-ar-game-with-ar-foundation-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can use ARFoundation to make your AR game in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/ar-basics/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about making AR experience in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
